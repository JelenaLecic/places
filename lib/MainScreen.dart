import 'package:flutter/material.dart';
import 'package:places/Place.dart';
import 'package:places/PlaceService.dart' as service;
import 'package:places/PlaceholderWidget.dart';
import 'package:places/PlacesList.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  MainScreenState createState() => new MainScreenState();
}

class MainScreenState extends State<MainScreen> {
  int _currentIndex = 0;
  BuildContext _context;

  List<Place> places;
  String error;

  List<Widget> _children = [
    Center(
      child: CircularProgressIndicator(),
    ),
  ];

  void onTabTapped(int index) {
    if (_children.length > 1) {
      setState(() {
        _currentIndex = index;
      });
    }
  }

  void onPlacesFetched(List<Place> places) {
    setState(() {
      this.places = places;
      _children = [
        //instantiate list
        PlacesList(places: places),
        //instantiate map
        PlaceholderWidget(Colors.deepOrange),
      ];
    });
  }

  void onError(dynamic error) {
    setState(() {
      Scaffold.of(_context).showSnackBar(new SnackBar(
        content: new Text(error),
        backgroundColor: Colors.red,
      ));
      _children = [
        PlaceholderWidget(Colors.white),
      ];
    });
  }

  @override
  void initState() {
    service.getLocation().then((Map<String, double> location) {
      service
          .fetchPlace(location['latitude'], location['longitude'])
          .then(onPlacesFetched);
    }).catchError(onError);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Places around me :)'),
      ),
      body: Builder(
        builder: (context) {
          _context = context;
          return _children[_currentIndex];
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped, // new
        currentIndex: _currentIndex, // new
        items: [
          new BottomNavigationBarItem(
            icon: Icon(Icons.list),
            title: Text('List'),
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.map),
            title: Text('Map'),
          ),
        ],
      ),
    );
  }
}
