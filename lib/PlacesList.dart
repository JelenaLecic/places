import 'package:flutter/material.dart';
import 'package:places/Place.dart';

class PlacesList extends StatelessWidget {
  List<Place> places;

  PlacesList({Key key, this.places}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: places.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text('${places[index].name}'),
        );
      },
    );
  }
}



