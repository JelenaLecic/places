import 'dart:collection';

class Place {
  final String name;
  final LinkedHashMap<String, dynamic> location;

  Place({this.name, this.location});

  factory Place.fromJson(Map<String, dynamic> json) {
    return Place(
      name: json['name'],
      location: json['location'],
    );
  }

  @override
  String toString() {
    print("$name");
    return super.toString();
  }
}