import 'package:flutter/material.dart';
import 'package:places/MainScreen.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: new ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: new MainScreen(title: 'Places Flutter App :)'),
    );
  }
}
