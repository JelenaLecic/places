import 'dart:async';
import 'dart:convert';

import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:places/Place.dart';

Future<List<Place>> fetchPlace(double latitude, double longitude) async {
  final String url =
      'https://api.foursquare.com/v2/venues/search?client_id=M5RIOMAXONJ5CMYJMBZ3222B3MQOFG4LZW4ROJVAPRHKH4XB&client_secret=HCATL24LZXN2SPGVPS0BXI5BTGCTHWZPQMPXPYHG3KNWXYKE&ll=$latitude,$longitude&v=20180904';
  final response = await http.get(url);

  if (response.statusCode == 200) {
    var responseJson = json.decode(response.body);
    return (responseJson['response']['venues'] as List)
        .map((p) => Place.fromJson(p))
        .toList();
  } else {
    throw Exception('Failed to load post');
  }
}

Future<Map<String, double>> getLocation() async {
  Map<String, double> fetchedLocation = new Map<String, double>();

  Position position =
      await Geolocator().getLastKnownPosition(LocationAccuracy.high);

  fetchedLocation['latitude'] = position.latitude;
  fetchedLocation['longitude'] = position.longitude;

  return fetchedLocation;
}
